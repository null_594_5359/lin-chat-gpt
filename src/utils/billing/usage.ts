import axios from 'axios';
import { useMessage } from '/@/hooks/web/useMessage';
import { getCurMonthFirstOrLast } from '../date/MonthStartDayAndEndUtil';

export const getUsage = async (apiKey: string): Promise<any> => {
  const { createErrorModal } = useMessage();
  const billingUrl = `https://api.openai.com/dashboard/billing/usage?end_date=${getCurMonthFirstOrLast(
    1,
  )}&start_date=${getCurMonthFirstOrLast(0)}`;
  axios.defaults.headers.get['Content-Type'] = 'application/json';
  axios
    .get(billingUrl, {
      headers: { Authorization: `Bearer ${apiKey}` },
    })
    .then((res) => {
      // @ts-ignore
      console.log(res.data);
      return Promise.resolve(res.data.usage);
    })
    .catch((error) => {
      console.log(error);
      createErrorModal({ title: '错误！', content: 'ApiKey无效' });
      return Promise.reject(error);
    });
  return Promise.reject();
};

export const billingUrl = `https://api.openai.com/dashboard/billing/usage?end_date=${getCurMonthFirstOrLast(
  1,
)}&start_date=${getCurMonthFirstOrLast(0)}`;
