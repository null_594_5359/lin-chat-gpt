import { defHttp } from '/@/utils/http/axios';

enum Api {
  SET_KEY = '/user/',
  USAGE_API_URL = 'https://api.openai.com/dashboard/billing/usage',
}

export const getUsage = () =>
  defHttp.get({
    url: Api.USAGE_API_URL,
  });
export const setApiKeyApi = (params: string) =>
  defHttp.get(
    {
      url: Api.SET_KEY + params,
    },
    {
      errorMessageMode: 'modal',
      successMessageMode: 'message',
    },
  );
