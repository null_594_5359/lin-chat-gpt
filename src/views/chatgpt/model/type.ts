interface Delta {
  role: string | undefined;
  content: string;
}
interface Record {
  question?: string;
  delta: Delta;
}
export interface RecordList {
  items: Record[];
}

export interface UsageTokens {
  promptTokens: number;
  completionTokens: number;
}
