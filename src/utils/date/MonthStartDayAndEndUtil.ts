/**
 * @description 获取当前月第一天 最后一天 当前天
 * @author xiahl
 * @html
 * @param  // type 0 第一天; 1 最后一天; 不传 当天;
 */
export function getCurMonthFirstOrLast(type = 0) {
  const curT: Date = new Date();
  const y = curT.getFullYear(); //获取年份
  let m: string | number = [curT.getMonth() + 1, curT.getMonth() + 2][type]; //获取月份
  // let d: string | number = [1, new Date(y, m, 0).getDate(), curT.getDate()][type];
  m = <string>(m < 10 ? '0' + m : m); //月份补 0
  // d = <string>(d < 10 ? '0' + d : d); //日数补 0
  const d = '01';
  return [y, m, d].join('-');
}
