import type { AppRouteModule } from '/@/router/types';

import { LAYOUT } from '/@/router/constant';
import { t } from '/@/hooks/web/useI18n';

const linChatGPT: AppRouteModule = {
  path: '/chat',
  name: 'Chat',
  component: LAYOUT,
  redirect: '/chat/index',
  meta: {
    icon: 'simple-icons:about-dot-me',
    title: t('routes.chat.chat'),
    orderNo: 10,
  },
  children: [
    {
      path: 'index',
      name: 'ChatPage',
      component: () => import('/@/views/chatgpt/chat.vue'),
      meta: {
        title: t('routes.chat.question'),
        icon: 'simple-icons:about-dot-me',
      },
    },
    {
      path: 'communication',
      name: 'CommunicationPage',
      component: () => import('/@/views/chatgpt/Action.vue'),
      meta: {
        icon: 'simple-icons:about-dot-me',
        title: t('routes.chat.context'),
      },
    },
    {
      path: 'developer',
      name: 'Developer',
      component: () => import('/@/views/chatgpt/Develops.vue'),
      meta: {
        icon: 'simple-icons:about-dot-me',
        title: t('routes.chat.developer'),
      },
    },
  ],
};

export default linChatGPT;
